#!/bin/bash


kubectl cluster-info | grep 'Kubernetes master' | awk '/http/ {print $NF}'

kubectl get secret <secret name> -o jsonpath="{['data']['ca\.crt']}" | base64 --decode




echo """
apiVersion: v1
kind: ServiceAccount
metadata:
  name: gitlab-admin
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: gitlab-admin
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: gitlab-admin
  namespace: kube-system
""" > sa.yaml

kubectl apply -f sa.yaml


kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep gitlab-admin | awk '{print $1}')