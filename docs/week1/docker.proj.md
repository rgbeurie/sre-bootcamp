# Project 1

!!! info inline end
    30 ~ 45 mins

Inside `src/sre/docker/` there's a folder `project1` that contains a nodeJS app that you need to containerize.

Some important things to note are:

- It listens on port 3000
- It requires express installed, so you need to run `npm install` or `yarn install` before you can run it, you can use the package.json to install the dependencies
- You can use any official [NodeJS docker image](https://hub.docker.com/_/node) version 14+

---
- [ ] Create a Dockerfile for the app
- [ ] Run the app and expose in port 3000
- [ ] Curl the app and make sure it works
  - [ ] curl on path `/` and save the response in a text file
  - [ ] curl on path `/health` and save the response in the same text file
  - [ ] curl on path `/whoami` and save the response in the same text file
