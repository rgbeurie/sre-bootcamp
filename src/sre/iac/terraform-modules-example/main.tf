module "random-pet" {
  source = "./random-stack"
}


#! issues: https://github.com/oracle/terraform-provider-oci/issues/1608
module "container-stack" {
  source = "./container-stack"
  container_count = 3
  image_name = "httpd"
}

#? this is who it should work https://learn.hashicorp.com/tutorials/terraform/module-create

