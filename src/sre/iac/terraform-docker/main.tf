module "containers" {
  source          = "./container-stack"
  container_count = 1
  image_name      = "nginx:1.23.1"
}
