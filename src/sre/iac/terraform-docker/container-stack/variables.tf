variable "image_name" {
  description = "input image name to use"
  default     = "nginx:latest"
}

variable "container_count" {
  description = "number of containers to create"
  default     = 1
}
