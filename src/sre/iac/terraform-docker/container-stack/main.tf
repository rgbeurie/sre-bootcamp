resource "docker_network" "private_network" {
  name   = "my_network"
  driver = "bridge"
}

# resource <resource_type> <resource_name(ID)>
resource "docker_image" "nginx" {
  name         = var.image_name
  keep_locally = false
}

# dependencies
resource "docker_container" "nginx" {

  count = var.container_count
  image = docker_image.nginx.name
  name  = "terraform-tutorial-${count.index}"

  ports {
    internal = 80
    external = "808${count.index}"
  }

  labels {
    label = "environment"
    value = "development"
  }

  labels {
    label = "image-txt"
    value = "The image is ${docker_image.nginx.name}"
  }

  networks_advanced {
    name = resource.docker_network.private_network.id
  }
}
