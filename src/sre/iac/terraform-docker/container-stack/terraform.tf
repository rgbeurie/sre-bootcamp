# * terraform block
# * like an import statement
terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "~> 2.13.0"
    }
  }
}



# * provider block
provider "docker" {
  host = "unix:///Users/beurie.reyes/.colima/default/docker.sock"
}
